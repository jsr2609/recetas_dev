# Tareas programadas

## Proceso cron

Cron es un demonio (que es como se conoce a un proceso en segundo plano). Para garantizar que nuestro sistema operativo esté configurado correctamente, es muy importante que obtenga la hora automáticamente,

## Archivo crontab

Crontab es un archivo de texto, se trata de un archivo con un contenido especial y específicamente diseñado para que sea leído correctamente por Cron y proceder con la ejecución que nosotros hayamos programado.

Cada valor en crontab cuenta con hasta cinco campos:

* Minuto: valores 0-59.
* Hora: valores 0-23.
* Día del mes: valores 1-31.
* Mes: valores 1-12.
* Día de la semana: valores 0-6. El 0 representa el domingo. Ten en cuenta que en algunos sistemas, el valor 7 representa el domingo.
* Comando: valores Orden a ejecutar.

Para crear el archivo ejecutar el comando  `crontab -e`.



## Configuración en contenedor


### Archivo entrypoint.sh

El archivo deberá contener  las instrucciones para iniciar el crond.

```
#!/bin/sh
crond -b
php-fpm -D
nginx -g "daemon off;"
```

Se tienen que agregar los permisos de ejcución en el Dockerfile `RUN chmod a+x /entrypoint.sh` y agregarlo como entrypoint `ENTRYPOINT ["/entrypoint.sh"]`.

## Prueba

Se puede agregar una línea de prueba en el dockerfile para comprobar el funcionamiento del cron.

`RUN echo "*       *       *       *       *      echo hola >> /test.txt" >> /var/spool/cron/crontabs/root`

La tarea anterior agrega una línea cada minuto en el archivo /test.txt, el archivo tiene que existir.

## Debug crontab Alpine

crond -f -l 2 -L /dev/stdout
