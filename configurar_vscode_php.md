# Configurar VSCode

## Extensiones

### Docker

### ENV

### GitLens

### PHP Awesome Snippets

### PHP Class Generator

Agregar en la configuración de la extensión el namespace `App`

### PHP Debug

Configurar la extensión para el proyecto.

### PHP Intelephense

Deshabilitar PHP Languaje Features de VSCode para evitar conflictos, en extensiones buscar `@builtin php` y deshabilitar.

### Scratchpads

### Twig Languaje

### Twig LAnguaje 2

### PHP Namespace Resolver

## Preferencias

Asignar las preferencias para los archivos de twig con la finalidad de implementar autocompletado y sintaxis.

### Bootstrap 4, Font awesome 4, Font Awesome 5 F





