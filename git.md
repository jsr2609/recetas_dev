 
## Ignorar un archivo con seguimiento previo

Primero agregarlo al .gitignore y ejecutar los siguiente comando y hacer un commit.

* `git rm --cached debug.log`

La opción `--cached` elimina el seguimiento en el repositorio y lo conserva en el directorio de trabajo.


## Eliminar rama del repositorio remoto

`git push origin --delete nombre-de-rama`

## Eliminar referencia a rama remota

`git remote prune origin` Elimina las referencias locales que ya no existen en un repositorio remoto.
