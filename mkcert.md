# MKCERT

Herramienta para crear certificados válidos para desarrollo local.
El proceso es instalar en el equipo local para que en los navegadores locales los certificados se detecten como válidos.

mkcert -install

Instala el certificador local y los archivos necesarios para los navegadores.

mkcert localhost example.com

Genera certificados en la carpeta donde se ejecuta el comando, los cuales se tienen que mover en la carpeta donde se tenga configurado el ssl en los servidors web.
