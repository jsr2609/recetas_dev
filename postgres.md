# POSTGRES
 
## Reiniciar secuencia
 
### Consultar el valor actual de la secuencia
 
 `SELECT last_value FROM sequence_name;`
 
### Consultar el valor más alto del id
 
`select max(id) from esquema.tabla;`

### Actualizar la secuencia

`ALTER SEQUENCE esquema.tabla_id_seq RESTART WITH 20;`

## Importar archivo csv

### Conectarse a la base de datos y realizar con el comando `copy`.

`copy esquema.tabla (id, campo_1, campo_2) FROM '/ruta/archivo.csv' DELIMITER ',' CSV HEADER QUOTE '"' ESCAPE '''';`

Realizar los cambios correspondientes de acuerdo al csv.

## Respaldar bd desde la línea de comandos

pg_dump --file "/ruta/nombre_bd.backup" --host "ip-host" --port "5432" --username "postgres" -W --verbose --format=c --blobs "nombre_bd"

## Restaurar bd desde la línea de comandos

pg_restore --host "ip-host" --port "5432" --username "postgres" -W --dbname "nombre_bd" --verbose "/ruta/nombre_bd.backup"
