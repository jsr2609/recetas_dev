# CERTBOT

## Directorios

Crear las siguientes carpetas en el directorio raiz del proyecto.

* letsencrypt

* nginx/conf.d

Dentro de las carpetas crear un archivo .gitkeep, el cual solo funciona para que esos directorios se suban al repositorio. Realizar un `git  commit`.

## gitignore

Agregar la carpetas que se crearon al archivo .gitignore

```
letsencrypt/*
nginx/conf.d/*
```

Realizar un `git commit`.

## docker-compose.yml

Habilitar los siguientes volumenes para el contenedor.

```
- ./letsencrypt:/etc/letsencrypt
- ./nginx/conf.d:/etc/nginx/conf.d
```

## Virtual host

Crear un virtual host para el dominio, agregando un nuevo archivo en `nginx/conf.d/nombre_dominio.conf`.

## Dockerfile

Agregar al Dockerfile la instrucción para instalar el certbot en la imagen.

`RUN apk add certbot certbot-nginx`

Construir la nueva imagen con el comando `make build`, eliminar el contenedor actual con el comando `docker container rm nombre_contenedor`. Correr nuevamente el contenedor con `make run`.


## Instalar certificados

Ingresar al contenedor con el siguiente comando `docker exec -it nombre_contenedor bash`.

Ejecutar el comando `certbot --nginx`, seguir las instrucciones y seleccionar el dominio correcto para solicitar los certificados.

### Renovar certificados

Renovar el certificado con el comando `certbot renew --quiet`.

### Renovar automáticamente certificados

En el Dockerdile se tiene que agregar que el crond inicie en el contenedor.

* Si hay un entrypoint agregar el comando `crond -b`
* Si es por CMD agregar en el Dockerdile `CMD exec crond -f & && php-fpm -D && nginx -g "daemon off;"`

Para automatizar la renovación, agregar la siguiente línea al Dockerfile:

`RUN echo "0       3       1       *       *      certbot renew --quiet" >> /var/spool/cron/crontabs/root`
