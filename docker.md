# Docker

## Instalación

https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-22-04

## Instalar docker-compose

https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-compose-on-ubuntu-22-04

## Generar un contendor temporal

`docker run --rm -d --name testphp -p 8090:80 jsr2609/symfony5.x-php7.4-fpm-alpine-dev`

* `--rm` Elimina el contenedor al cerrarlo
