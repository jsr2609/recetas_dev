# Firma Electrónica

## OpenSSL

### CipherString

`/etc/ssl/openssl.cnf`

CipherString = DEFAULT@SECLEVEL=2

to

CipherString = DEFAULT@SECLEVEL=1

### Convertir .cer a pem

*Advertencia:* Si se copian los comandos de la guía del PDF, algunos `-` no se pasan bien, se pega un caracter parecido y manda error.

`openssl x509 -in gava730717ae1.cer -inform der -outform pem -out gava730717ae1.pem`


### Comprobar la firma

`openssl ocsp -issuer AC_UAT_2022.crt -cert gava730717ae1.pem -text -url https://cfdit.sat.gob.mx/edofiel -VAfile OCSP_UAT_2022.crt -header host=cfdit.sat.gob.mx`

Donde:

* ocsp indica que se trata de una sentencia de OCSP
* -issuer es para indicarle que a continuación habrá un certificado de AC o raíz
* -url antecede a la dirección del sitio que alberga el servicio de OCSP
* -cert es para indicarle que a continuación habrá el certificado del cual se desea
conocer su estado
* -VAfile indica que se verificara por el OCSP y se incluye el certificado de respuesta
OCSP
