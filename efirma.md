# digestion o sign
OpenSSL> dgst -sha256 -sign c:/test/gava730717ae1key.pem -out c:/test/cadena.sig c:/test/cadena.txt

Basicamente son estos los comandos utilizados:
## Conversion de key a pem
 OpenSSL> pkcs8 -inform DER -in /home/jsr/Documents/FIEL_SARJ840926JY6_20200804121358/DFEC100101HGRMMR01.key -out /home/jsr/Documents/FIEL_SARJ840926JY6_20200804121358/DFEC100101HGRMMR01_KEY.pem
 
  OpenSSL> pkcs8 -inform DER -in /home/jsr/Documents/FIEL_SARJ840926JY6_20200804121358/SARJ840926JY6.key -out /home/jsr/Documents/FIEL_SARJ840926JY6_20200804121358/SARJ840926JY6_KEY.pem

-- conversion de cert a pem
OpenSSL> x509 -inform DER -outform PEM -in c:/test/gava730717ae1.cer -pubkey -out c:/test/gava730717ae1pubkey.pem

-- obtener seral de cert
OpenSSL> x509 -in c:/test/gava730717ae1.pem -noout -serial

-- digestion o sign
OpenSSL> dgst -sha256 -sign c:/test/gava730717ae1key.pem -out c:/test/cadena.sig c:/test/cadena.txt

-- conversion a base64
OpenSSL> base64 -e -in c:/test/cadena.sig -out c:/test/cadenasig64.txt

-- verificacion de cadena original
OpenSSL> dgst -sha256 -verify c:/test/gava730717ae1pubkey.pem -signature c:/test/cadena.sig c:/test/cadena.txt
